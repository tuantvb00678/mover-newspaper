<?php if ( !empty($title) || !empty($list) ): ?>
<div class="con_pack_tuition">
  <h3 class="st_title"><span><?php echo $title; ?></span></h3>
  <div class="container">
    <div class="box_pack_tuition">
      <?php if ( !empty($list) ): ?>
        <div class="row">
          <?php foreach($list as $item): ?>
            <div class="col-xs-12 col-sm-4">
              <div class="box_item <?php echo $item['box_class']; ?>">
                <div class="box_pheader">
                  <h4><?php echo $item['title']; ?></h4>
                </div>
                <div class="box_info">
                  <h4 class="price"><?php echo $item['new_price']; ?> <br /><span><?php echo $item['old_price']; ?></span></h4>
                  <ul>
                    <li><?php _e('Số buổi học:', 'indentalchesterhill'); ?> <span><?php echo $item['number']; ?></span></li>
                    <li><?php _e('Thời gian:', 'indentalchesterhill'); ?>  <span><?php echo $item['time']; ?></span></li>

                    <?php if ( isset($item['other_options']) && !empty($item['other_options']) ): ?>
                      <?php foreach($item['other_options'] as $other_option): ?>
                        <li><?php echo $other_option; ?></li>
                      <?php endforeach; ?>
                    <?php endif; ?>
                  </ul>
                  <?php $url = (!empty($item['register_url'])) ? $item['register_url']: '#'; ?>
                  <a href="<?php echo $url; ?>" class="btn <?php echo $item['btn_class']; ?>"><?php _e('Đăng ký học thử', 'indentalchesterhill'); ?></a>
                </div>
              </div>
            </div>
          <?php endforeach; ?>
        </div>
      <?php endif; ?>
    </div>
  </div>
</div>
<?php endif; ?>
