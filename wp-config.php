<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', "mover" );

/** MySQL database username */
define( 'DB_USER', "root" );

/** MySQL database password */
define( 'DB_PASSWORD', "" );

/** MySQL hostname */
define( 'DB_HOST', "localhost" );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',          'Up~j~,A;a(bA4tCTQl]&-WNuEr6]&?J}cCW9oxWa]E$B4Nu[7$$8fiP } >{tdVW' );
define( 'SECURE_AUTH_KEY',   'wnu##D$t]-/4`T739Uh`ptDgDUGj.|yX/C6Ml~wpd@#t/,ze39F@hF(fjPiR (S&' );
define( 'LOGGED_IN_KEY',     '(rc:kA@wy<$Fw 1-ZYs/q88@C}O3xXg(G=,`s1`g{IC@DNZ+68m)n[o/Tlz=bprB' );
define( 'NONCE_KEY',         'y5U*|jGodJu2G58RM(v!4`T/AD8XzM3TjuI?|+:m7Byg=(61k^+Qq@hsL9L5c:zl' );
define( 'AUTH_SALT',         '|383v)M:Ni[Hl7!g0izR87> fv&N=W^kJK_^AW!gTP_$r%ih;SX(^Bx5axOdkJ+:' );
define( 'SECURE_AUTH_SALT',  '3#WwnP}<.1/M$IBHmT.l5|(V#OM4O0y%.INgH }z^im/0495g%tY,&P8%_EeFy^<' );
define( 'LOGGED_IN_SALT',    '%v7.]fYR!)g4@L1Et`ypIJ[p29UAf~Kq2 A#5pU$L(@# <(8{u>k49.a?YCQ^2C}' );
define( 'NONCE_SALT',        '^WQqnA9x.Sin22rWbbOD|<F.MzjXZRjNy{i/TFmrc8]Jo4J^Xw+S&0b:+X=$qRQL' );
define( 'WP_CACHE_KEY_SALT', '<QsDx,WcXj&q=..)=?yix>8L$LNd7,^#J7PEDdD1AEf3Xzg+9S3vemRru!<w]gdW' );

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
