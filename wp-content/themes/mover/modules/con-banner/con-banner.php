<?php if( !empty($list) ): ?>
  <div class="con_banner">
    <div class="container-wrap">
      <div class="box_banner">
        <?php foreach($list as $item): ?>
          <div><img src="<?php echo $item['url']; ?>" alt="" /></div>
        <?php endforeach; ?>
      </div>
    </div>
  </div>
<?php endif; ?>
