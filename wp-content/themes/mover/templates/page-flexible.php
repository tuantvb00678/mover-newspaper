<?php
/*
Template Name: Flexible
Template Post Type: post, page
*/
get_header();

the_module('hero');

if (have_posts()) {

  while (have_posts()) :

    the_post();
    $title = get_the_title();
    $background_color = get_field('hero_background_color');
    $class = 'hero-title--page-default';
    if ($background_color == 'orange') {
      $class .= ' hero-title--orange-background';
    }

    the_module('hero-title', array(
      'headline' => $title,
      'title' => get_field('hero_headline'),
      'description' => get_field('hero_description'),
      'class' => $class
    ));


    $list = swe_get_testimonials('program_testimonials_list');
    the_module('testimonial-list', array(
      'title' => 'Student Testimonials',
      'class' => 'testimonial-list',
      'list' => $list
    ));

  endwhile;

}

get_footer();
?>
