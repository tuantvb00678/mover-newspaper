<?php if ( !empty($title) || !empty($list) ): ?>
<div class="con_reviewer">
  <div class="container">
    <h3 class="st_title"><span><?php echo $title; ?></span></h3>
    <?php if( !empty($list) ): ?>
      <div class="box_list_reviewer">
        <div class="box_slider">
          <?php foreach($list as $item): ?>
            <div class="box_item">
              <div class="box_intro">
                <div><?php echo $item['content']; ?></div>
              </div>
              <div class="box_teacher">
                <p class="photo">
                  <img src="<?php echo $item['image']; ?>" alt="<?php echo $item['title']; ?>" />
                </p>
                <h3><?php echo $item['title']; ?>
                  <span><?php echo $item['age']; ?></span>
                </h3>
              </div>
            </div>
          <?php endforeach; ?>
        </div>
      </div>
    <?php endif; ?>
  </div>
</div>
<?php endif; ?>
