<?php
  get_header();
  global $wp_query;
  ?>
  <section class="con_list_news con_list_news--category">
    <div class="container">
      <h3 class="st_title"><span><?php the_title(); ?></span></h3>
      <div class="box_news">
        <div class="row">
          <?php
            $cols = 3;
            $rowCount = 0;
            if (have_posts()) { 
              while ( have_posts() ) { 
                the_post();
                $obj = get_post(get_the_ID());
                $item_post = get_item_news($obj);
                the_module('con-new', array(
                  'item' => $item_post
                ));
                $rowCount++;
                if($rowCount % $cols == 0 && $rowCount < $wp_query->found_posts) echo '</div><div class="row">';
              }
              wp_reset_postdata();
            }
          ?>
        </div>
      </div>
      <div class="pagination"><?php wp_pagination(); ?></div>
    </div>
  </section>
<!-- end secsion box news -->
  <?php
  get_footer();
?>
