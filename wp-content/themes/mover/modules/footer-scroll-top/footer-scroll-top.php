<div class="scroll_top visible-md visible-lg visible-sm">
  <a href="#main">
    <i class="fas fa-arrow-up"></i>
  </a>
</div>
<div class="con_present" id="present">
  <div class="box_present">
    <div class="box_info">
      <?php if ( !empty($title) ): ?>
        <h4><?php echo $title; ?></h4>
      <?php endif; ?>
      <div class="con_present-form js-form-phone">
        <?php if ( !empty($form_id) ) :
          echo do_shortcode( '[contact-form-7 id="'.$form_id.'"]' );
        endif; ?>
      </div>
    </div>
    <?php $image_url = THEME_URI . "/assets/images/img_present.png";?>
    <p class="photo"><img src="<?php echo $image_url; ?>" alt="<?php echo $title; ?>" /></p>
  </div>
</div>
