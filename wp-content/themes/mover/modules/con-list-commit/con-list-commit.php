<?php if ( !empty($title) || !empty($content) || !empty($image) ): ?>
<!-- start secsion Commitment -->
<div class="con_list_commit">
  <div class="container">
    <div class="box_list_commit">
      <div class="row">
        <div class="col-xs-12 col-sm-6">
          <p class="photo_intro">
            <img src="<?php echo $image['url']?>" alt="<?php echo $title; ?>" />
          </p>
        </div>
        <div class="col-xs-12 col-sm-6">
          <div class="box_intro">
            <h3 class="st_title_left"><span><?php echo $title; ?></span></h3>
            <?php if ( !empty($content) ): ?>
            <ul>
              <?php foreach($content as $item): ?>
              <li>
                <div class="box_inner">
                  <p class="photo">
                    <img src="<?php echo $item['content_image']['url']; ?>" alt="<?php echo $item['content_item']; ?>" />
                  </p>
                  <h4><?php echo $item['content_item']; ?></h4>
                </div>
              </li>
              <?php endforeach; ?>
            </ul>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
<!-- start secsion Commitment -->
<?php endif; ?>
