$(function() {
	$('.box_slider').slick({
	  // infinite: true,
	  arrows: true,
	  infinite: true,
	  slidesToShow: 3,
	  slidesToScroll: 3,
	  speed: 3000,
	  autoplay: true,
	  responsive: [
	    {
	      breakpoint: 1024,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 3,
	        infinite: true,
	        dots: true
	      }
	    },
	    {
	      breakpoint: 600,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    }
	    // You can unslick at a given breakpoint now by adding:
	    // settings: "unslick"
	    // instead of a settings object
	  ]
	});
	$('.box_banner').slick({
	  // infinite: true,
	  arrows: false,
	  infinite: true,
	  slidesToShow: 5,
	  // slidesToScroll: 5,
	  responsive: [
	    {
	      breakpoint: 1024,
	      settings: {
	        slidesToShow: 5,
	        slidesToScroll: 5,
	        infinite: true,
	        dots: true
	      }
	    },
	    {
	      breakpoint: 600,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2
	      }
	    }
	    // You can unslick at a given breakpoint now by adding:
	    // settings: "unslick"
	    // instead of a settings object
	  ]
	});
	$('.box_list_teacher').slick({
	  // infinite: true,
	  // arrows: false,
	  infinite: true,
	  slidesToShow: 1,
	  speed: 3000,
	  autoplay: true,
	  dots: true,
	  arrows: true,

	});

	// $('.box_slide_main').slick({
	// 	  // infinite: true,
	// 	  arrows: false,
	// 	  dots: true,
	// 	  infinite: true,
	// 	  speed: 500,
	// 	  fade: true,
	// 	  cssEase: 'linear'
	// 	});

});
$(document).ready(function () {
	$('#no1_mommenu .btn2.offcanvas').on('click', function(){
        if($('#menu_offcanvas').hasClass('active')){
            $(this).find('.overlay').fadeOut(250);
            $('#menu_offcanvas').removeClass('active');
            $('body').removeClass('show-sidebar');
        } else {
            $('#menu_offcanvas').addClass('active');
            $(this).find('.overlay').fadeIn(250);
            $('body').addClass('show-sidebar');
        }
    });
    $("#jsDownloadBtn ").click(function(){
	    $("#jsHeaderDownload").hide();
	    $('#page').addClass('noDowload');
	  });
    $('.dropdown-language').click(function(e) {
    	// alert($(this).getClass().getName());
	    e.stopPropagation();
	});
    $(".dropdown-language li").click(function(){
	    var class_lang = $(this).prop("class");
	    if($(".lnk_lang").hasClass("lag_en")) {
	    	$(".lnk_lang").removeClass("lag_en");
	    }
	    if($(".lnk_lang").hasClass("lag_vn")) {
	    	$(".lnk_lang").removeClass("lag_vn");
	    }

	    $(".lnk_lang").addClass(class_lang);
	    // $(".lnk_lang").attr('class',"test")
	  });

});
jQuery(document).ready(function(){
        /*Animate when click anchor link*/
        jQuery('a[href^="#"]').on('click', function(event) {
            var target = jQuery(this.getAttribute('href'));
            if( target.length ) {
                event.preventDefault();
                jQuery('html, body').stop().animate({
                    scrollTop: target.offset().top
                }, 1000);
            }
        });

        var form_wraper = jQuery('.box_signup form p');
        if(form_wraper) {
          var form = jQuery('.box_signup form');
          jQuery.each( form_wraper, function( index, item ) {
            if (index < 2) {
              jQuery(item).addClass('col-xs-12 col-sm-6');
            } else {
              jQuery(item).addClass('col-xs-12');
            }
          });
        }

        var inputs = jQuery('.box_signup input, textarea');
        jQuery.each(inputs, function(index, item){
          var type = jQuery(item).attr('type');
          switch(type) {
            case 'submit':
              jQuery(item).addClass('btn btn-lg');
              break;
            case 'text':
              jQuery(item).addClass('form-control');
              if(index == 5) {
                jQuery(item).attr('placeholder', 'Họ và Tên');
              } else {
                jQuery(item).attr('placeholder', 'Số Điện Thoại')
              }
              break;
            default:
              jQuery(item).addClass('form-control');
              jQuery(item).attr('rows', '5')
              jQuery(item).attr('placeholder', 'Để lại lời nhắn...')
              break;
          }
        });

        // js-form-phone at footer
        var phoneInputs = jQuery('.js-form-phone input');
        jQuery.each(phoneInputs, function(index, item) {
          var type = jQuery(item).attr('type');
          switch(type) {
            case 'submit':
              jQuery(item).addClass('btn btn-lg');
              break;
            case 'text':
              jQuery(item).addClass('form-control');
              jQuery(item).attr('placeholder', 'Số Điện Thoại')
              break;
          }
        });

        // js-form-con-main-phone at hero
        var phoneInputs = jQuery('.js-form-con-main-phone input');
        jQuery.each(phoneInputs, function(index, item) {
          var type = jQuery(item).attr('type');
          switch(type) {
            case 'submit':
              jQuery(item).addClass('btn btn-ssm white');
              break;
            case 'text':
              jQuery(item).addClass('form-control');
              jQuery(item).attr('placeholder', 'Số Điện Thoại')
              break;
          }
        });

});
