<?php if ( !empty($title) || !empty($form_id)): ?>
<!-- start secsion sign up -->

<div class="con_signup">
  <h3 class="st_title"><span><?php echo $title; ?></span></h3>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2">
        <div class="box_signup">
          <?php if ( !empty($form_id) ) :
            echo do_shortcode( '[contact-form-7 id="'.$form_id.'"]' );
          endif; ?>
        </div>
      </div>
    </div>
    <p class="photo_bottom">
      <?php $image_url = THEME_URI . "/assets/images/img_singup_intro.png";?>
      <img src="<?php echo $image_url; ?>" alt="<?php echo $title; ?>" />
    </p>
  </div>
</div>
<!-- end secsion sign up -->
<?php endif; ?>
