<?php

define('TAGDIV_ROOT', get_template_directory_uri());
define('TAGDIV_ROOT_DIR', get_template_directory());


// load the deploy mode
require_once( TAGDIV_ROOT_DIR . '/tagdiv-deploy-mode.php' );


/**
 * Theme configuration.
 */
require_once TAGDIV_ROOT_DIR . '/includes/tagdiv-config.php';


/**
 * Theme wp booster.
 */
require_once( TAGDIV_ROOT_DIR . '/includes/wp-booster/tagdiv-wp-booster-functions.php');


/**
 * Theme page generator support.
 */
if ( ! class_exists('tagdiv_page_generator' ) ) {
	include_once ( TAGDIV_ROOT_DIR . '/includes/tagdiv-page-generator.php');
}


/* ----------------------------------------------------------------------------
 * Add theme support for sidebar
 */
add_action( 'widgets_init', function() {
    register_sidebar(
        array(
            'name'=> 'Newspaper default',
            'id' => 'td-default',
            'before_widget' => '<aside id="%1$s" class="widget %1$s %2$s">',
            'after_widget' => '</aside>',
            'before_title' => '<div class="block-title"><span>',
            'after_title' => '</span></div>'
        )
    );
});


/**
 * Theme setup.
 */
add_action( 'after_setup_theme', function (){

	/**
	 * Loads the theme's translated strings.
	 */
	load_theme_textdomain( strtolower(TD_THEME_NAME ), get_template_directory() . '/translation' );

	/**
	 * Theme menu location.
	 */
	register_nav_menus(
		array(
			'header-menu' => 'Header Menu (main)',
			'footer-menu' => 'Footer Menu',
		)
	);
});


/* ----------------------------------------------------------------------------
 * Add theme support for features
 */
add_theme_support('title-tag');
add_theme_support('post-thumbnails');
add_theme_support('automatic-feed-links');
add_theme_support('html5', array('comment-list', 'comment-form', 'search-form', 'gallery', 'caption'));
add_theme_support('woocommerce');
add_theme_support('bbpress');
add_theme_support('align-wide');
add_theme_support('align-full');


global $content_width;
if ( !isset($content_width) ) {
    $content_width = 696;
}



/* ----------------------------------------------------------------------------
 * Woo Commerce
 */
// breadcrumb
add_filter('woocommerce_breadcrumb_defaults', 'tagdiv_woocommerce_breadcrumbs');
function tagdiv_woocommerce_breadcrumbs() {
    return array(
        'delimiter' => ' <i class="td-icon-right td-bread-sep"></i> ',
        'wrap_before' => '<div class="entry-crumbs" itemprop="breadcrumb">',
        'wrap_after' => '</div>',
        'before' => '',
        'after' => '',
        'home' => _x('Home', 'breadcrumb', 'newspaper'),
    );
}

// Number of product per page 4
add_filter('loop_shop_per_page', 'tagdiv_wc_loop_shop_per_page' );
function tagdiv_wc_loop_shop_per_page($cols) {
    return 4;
}

// use own pagination
if (!function_exists('woocommerce_pagination')) {
    // pagination
    function woocommerce_pagination() {
        tagdiv_page_generator::get_pagination();
    }
}

if (!function_exists('woocommerce_output_related_products')) {
    // Number of related products
    function woocommerce_output_related_products() {
        woocommerce_related_products(array(
            'posts_per_page' => 4,
            'columns' => 4,
            'orderby' => 'rand',
        )); // Display 4 products in rows of 1
    }
}





/* ----------------------------------------------------------------------------
* front end css files
*/
if( !function_exists('tagdiv_theme_css') ) {
    function tagdiv_theme_css() {
        wp_enqueue_style('td-theme', get_stylesheet_uri() );

        // load the WooCommerce CSS only when needed
        if ( class_exists('WooCommerce', false) ) {
            wp_enqueue_style('td-theme-woo', get_template_directory_uri() . '/style-woocommerce.css' );
        }

        // load the Bbpress CSS only when needed
        if ( class_exists('bbPress', false) ) {
            wp_enqueue_style('td-theme-bbpress', get_template_directory_uri() . '/style-bbpress.css' );
        }
    }
}
add_action('wp_enqueue_scripts', 'tagdiv_theme_css', 1001);








add_filter('upgrader_clear_destination', function($removed, $local_destination, $remote_destination, $args) {
    usleep(500);
    return $removed;
}, 10, 4);


if ( defined('TD_COMPOSER' ) && strpos( td_util::get_registration(), chr(42 ) ) > 0 ) {

    if ( is_admin()) {

        $value = get_transient( 'td_update_theme_' . TD_THEME_NAME );
        if ( false === $value ) {

            tagdiv_check_theme_version();

        } else {

            $td_theme_update_to_version = get_transient( 'td_update_theme_to_version_' . TD_THEME_NAME );
            if ( false !== $td_theme_update_to_version ) {
                $theme_update_to_version = tagdiv_util::get_option( 'theme_update_to_version' );

                if ( ! empty( $theme_update_to_version ) ) {

                    add_filter( 'pre_set_site_transient_update_themes', function( $transient ) {

                        $to_version = tagdiv_util::get_option( 'theme_update_to_version' );
                        if ( ! empty( $to_version )) {
                            $args = array();
                            $to_version = json_decode( $to_version, true );
                            $to_version_keys = array_keys( $to_version );
                            if ( is_array( $to_version_keys ) && count( $to_version_keys ) ) {
                                $to_version_serial = $to_version_keys[ 0 ];
                                $to_version_url = $to_version[$to_version_serial];
                                $theme_slug = get_template();

                                $transient->response[ $theme_slug ] = array(
                                    'theme'       => $theme_slug,
                                    'new_version' => $to_version_serial,
                                    'url' => "https://tagdiv.com/" . TD_THEME_NAME,
                                    'clear_destination' => true,
                                    'package'     => add_query_arg( $args, $to_version_url ),
                                );
                            }
                        }

                        return $transient;
                    });
                    delete_site_transient('update_themes');
                }
            } else {

                $td_theme_update_latest_version = get_transient( 'td_update_theme_latest_version_' . TD_THEME_NAME );

                if ( false !== $td_theme_update_latest_version ) {

                    add_filter( 'pre_set_site_transient_update_themes', function( $transient ) {

                        $latest_version = tagdiv_util::get_option( 'theme_update_latest_version' );
                        if ( ! empty( $latest_version ) ) {
                            $args = array();
                            $latest_version = json_decode( $latest_version, true );

                            $latest_version_keys = array_keys( $latest_version );
                            if ( is_array( $latest_version_keys ) && count( $latest_version_keys ) ) {
                                $latest_version_serial = $latest_version_keys[ 0 ];
                                $latest_version_url = $latest_version[$latest_version_serial];
                                $theme_slug = get_template();

                                $transient->response[ $theme_slug ] = array(
                                    'theme' => $theme_slug,
                                    'new_version' => $latest_version_serial,
                                    'url' => "https://tagdiv.com/" . TD_THEME_NAME,
                                    'clear_destination' => true,
                                    'package' => add_query_arg( $args, $latest_version_url ),
                                );
                            }
                        }

                        return $transient;
                    });
                    delete_site_transient('update_themes');
                }
            }
        }
    }


    add_filter( 'admin_body_class', function ( $classes ) {
		$new_update_available = false;
        $latest_version = tagdiv_util::get_option( 'theme_update_latest_version' );

        if ( ! empty( $latest_version ) ) {
            $latest_version = json_decode( $latest_version, true );

            $latest_version_keys = array_keys( $latest_version );
            if ( is_array( $latest_version_keys ) && count( $latest_version_keys ) ) {
                $latest_version_serial = $latest_version_keys[ 0 ];

                if ( 1 == version_compare( $latest_version_serial, TD_THEME_VERSION ) ) {
                    $new_update_available = true;
                }
            }
        }

        if ( $new_update_available ) {
            $classes .= ' td-theme-update';
        }

		return $classes;
	} );


	add_filter( 'admin_head', function () {

		$td_update_theme_ready = get_transient( 'td_update_theme_' . TD_THEME_NAME );
		if ( false !== $td_update_theme_ready ) {

			$update_data = '';

			$td_theme_update_to_version = get_transient( 'td_update_theme_to_version_' . TD_THEME_NAME );
			if ( false !== $td_theme_update_to_version ) {

				$data = tagdiv_util::get_option( 'theme_update_to_version' );
				if ( ! empty( $data ) ) {
					$update_data = $data;
				}
			} else {

				$data = tagdiv_util::get_option( 'theme_update_latest_version' );
				if ( ! empty( $data ) ) {
					$update_data = $data;
				}
			}

			if ( ! empty( $update_data ) ) {
				ob_start();
				?>
                <script>
                    var tdData = {
                        version: <?php echo $update_data ?>,
                        adminUrl: "<?php echo admin_url() ?>",
                        themeName: "<?php echo TD_THEME_NAME ?>",
                    };
                </script>
				<?php
				echo ob_get_clean();
			}
		}
	} );
}


add_action('upgrader_process_complete', function($upgrader, $data) {

    if ($data['action'] == 'update' && $data['type'] == 'theme' ) {

         // clear flag to update theme
        delete_transient( 'td_update_theme_' . TD_THEME_NAME );

        // clear flag to update theme to latest version
        delete_transient( 'td_update_theme_latest_version_' . TD_THEME_NAME );

        // clear flag to update theme to specific version
        delete_transient( 'td_update_theme_to_version_' . TD_THEME_NAME );

        // clear flag to update to a specific version
        tagdiv_util::update_option( 'theme_update_to_version', '' );

        $current_deactivated_plugins = tagdiv_options::get_array('td_theme_deactivated_current_plugins' );

        if ( ! empty( $current_deactivated_plugins ) ) {
            $theme_setup = tagdiv_theme_plugins_setup::get_instance();
            $theme_setup->theme_plugins( array_keys( $current_deactivated_plugins ) );

            ob_start();

            ?>

            <script>
                setTimeout(function(){
                    jQuery('.td-button-install-plugins').trigger('click');
                }, 1000);
            </script>

            <?php

            echo ob_get_clean();
        }
    }

}, 10, 2);


add_filter('upgrader_pre_install', function( $return, $theme) {
    if ( is_wp_error( $return ) ) {
        return $return;
    }

    $theme = isset( $theme['theme'] ) ? $theme['theme'] : '';

    if ( $theme != get_stylesheet() ) { //If not current
        return $return;
    }

    tagdiv_options::update_array( 'td_theme_deactivated_current_plugins', '' );
    $deactivation = new tagdiv_current_plugins_deactivation();
    $deactivation->td_deactivate_current_plugins( true );

    return $return;

}, 10, 2);



add_action( 'current_screen', function() {
    $current_screen = get_current_screen();

    if ( 'update-core' === $current_screen->id && isset( $_REQUEST['update_theme'] )) {

        add_action('admin_head', function() {

            $theme_name = $_REQUEST['update_theme'];

            ob_start();
            ?>

            <script>
                jQuery(window).ready(function() {

                    'use strict';

                    var $formUpgradeThemes = jQuery('form[name="upgrade-themes"]');
                    if ( $formUpgradeThemes.length ) {
                        var $input = $formUpgradeThemes.find('input[type="checkbox"][value="<?php echo $theme_name ?>"]');
                        if ($input.length) {
                            $input.attr( 'checked', true );
                            $formUpgradeThemes.submit();
                        }
                    }
                });
            </script>

            <?php
            echo ob_get_clean();
        });
    }
});

// [pdf link="https://drive.google.com/file/d/0B7PqGrg9aj4ybmh3Sm42NmxhZnM/preview" width="550", height="550"]
function pdf_shortcode_func( $atts ) {
	$a = shortcode_atts( array(
		'link' => 'link',
		'width' => 'width',
		'height' => 'height'
	), $atts );

	return '<iframe src="'.$a['link'].'" width="100%" height="600" frameborder="0" marginheight="0" marginwidth="0">
				<p style="font-size: 110%;"><em><strong>ERROR: </strong> An &#105;frame should be displayed here but your browser version does not support &#105;frames.</em> Please update your browser to its most recent version and try again, or access the file <a href="'.$a['link'].'"with this link.</a></p>
			</iframe>';
}
add_shortcode( 'pdf', 'pdf_shortcode_func' );


// [google_form_url link="https://docs.google.com/forms/d/e/1FAIpQLSdMDtuOcqwJJgxVmlcqp3n5jIn08_EOJ3146GtiFTunpR6UGg/viewform?embedded=true"]
function google_form_url_shortcode_func( $atts ) {
	$a = shortcode_atts( array(
		'link' => 'link',
	), $atts );

	return '<iframe id="frame" src="'.$a['link'].'" width="100%" height="600" frameborder="0" marginheight="0" marginwidth="0">Loading...</iframe>';
}
add_shortcode( 'google_form_url', 'google_form_url_shortcode_func' );


/*Add this code to your functions.php file of current theme OR plugin file if you're making a plugin*/
//add the button to the tinymce editor
add_action('media_buttons_context','add_my_tinymce_media_button');
function add_my_tinymce_media_button($context){
    return $context.=__("
        <a href=\"#TB_inline?width=480&inlineId=my_shortcode_popup&width=640&height=513\" class=\"button thickbox\" id=\"my_shortcode_popup_button\" title=\"PDF Content\">PDF Content</a>
        <a href=\"#TB_inline?width=480&inlineId=google_shortcode_popup&width=640&height=513\" class=\"button thickbox\" id=\"google_shortcode_popup_button\" title=\"Google Form URL\">Google Form URL</a>
    ");
}

add_action('admin_footer','my_shortcode_media_button_popup');
//Generate inline content for the popup window when the "my shortcode" button is clicked
function my_shortcode_media_button_popup(){?>
  <div id="my_shortcode_popup" style="display:none;">
    <div class="wrap">
      <div>
        <h2>Add PDF Preview</h2>
        <div class="my_shortcode_add">
          <input type="text" id="id_of_textbox_user_typed_in" size="70px">
		  <button class="button-primary" id="id_of_button_clicked">Add URL</button>
        </div>
      </div>
    </div>
  </div>
  <!-- Google form popup -->
  <div id="google_shortcode_popup" style="display:none;">
    <div class="wrap">
      <div>
        <h2>Add Google Form Url</h2>
        <div class="google_shortcode_add">
          <input type="text" id="google_input_url" size="70px">
		  <button class="button-primary" id="google_button_clicked">Add URL</button>
        </div>
      </div>
    </div>
  </div>
<?php
}

//javascript code needed to make shortcode appear in TinyMCE edtor
add_action('admin_footer','my_shortcode_add_shortcode_to_editor');
function my_shortcode_add_shortcode_to_editor(){?>
<script>
jQuery('#id_of_button_clicked ').on('click',function(){
  var user_content = jQuery('#id_of_textbox_user_typed_in').val();
  var shortcode = '[pdf link="'+user_content+'"/]';
  if( !tinyMCE.activeEditor || tinyMCE.activeEditor.isHidden()) {
    jQuery('textarea#content').val(shortcode);
  } else {
    tinyMCE.execCommand('mceInsertContent', false, shortcode);
  }
  //close the thickbox after adding shortcode to editor
  self.parent.tb_remove();
});
// google button clicked
jQuery('#google_button_clicked ').on('click',function() {
  var user_content = jQuery('#google_input_url').val();
  var shortcode = '[google_form_url link="' + user_content + '"/]';
  if( !tinyMCE.activeEditor || tinyMCE.activeEditor.isHidden()) {
    jQuery('textarea#content').val(shortcode);
  } else {
    tinyMCE.execCommand('mceInsertContent', false, shortcode);
  }
  //close the thickbox after adding shortcode to editor
  self.parent.tb_remove();
});
</script>
<?php
}
