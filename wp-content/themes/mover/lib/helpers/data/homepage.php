<?php
/*
* Get data for Home page
*/

function get_list_reasons($list)
{
  $items = [];
  if (!empty($list) ) {
    foreach ($list as $item) {
      $items[] = array(
        'title' => $item->post_title,
        'url' => get_permalink( $item->ID ),
        'image' => get_the_post_thumbnail_url($item->ID,'full')
      );
    }
  }
  return $items;
}

function get_list_teachers($list)
{
  $items = [];
  if (!empty($list) ) {
    foreach ($list as $item) {
      $ratting = get_field('teacher_ratting', $item->ID);
      $items[] = array(
        'title' => $item->post_title,
        'content' => $item->post_content,
        'url' => get_permalink( $item->ID ),
        'image' => get_the_post_thumbnail_url($item->ID,'full'),
        'score' => get_field('teacher_score', $item->ID),
        'ratting' => $ratting,
        'width' => $ratting*20,
      );
    }
  }
  return $items;
}

function get_list_packages($list)
{
  $items = [];
  if (!empty($list) ) {
    $box_class = array('default', 'middle', 'right');
    $btn_class = array('btn-lg', 'btn-lg2', 'btn-lg');
    foreach ($list as $key=>$item) {
      $other_options = array();
      $package_other_options = get_field('package_other_option', $item->ID);
      if ( !empty($package_other_options) ) {
        foreach($package_other_options as $option) {
          $other_options[] = $option['option_item'];
        }
      }
      $items[] = array(
        'box_class' => $box_class[$key%3],
        'btn_class' => $btn_class[$key%3],
        'title' => $item->post_title,
        'new_price' => get_field('package_new_price', $item->ID),
        'old_price' => get_field('package_old_price', $item->ID),
        'number' => get_field('package_number_of_sessions', $item->ID),
        'time' => get_field('package_time', $item->ID),
        'other_options' => $other_options,
        'register_url' => get_field('package_register', $item->ID),
      );
    }
  }
  return $items;
}

function get_list_courses($list)
{
  $items = [];
  $images_url = THEME_URI . "/assets/images/";
  $number_img = array('ic_num1.png', 'ic_num2.png', 'ic_num3.png', 'ic_num4.png');
  if (!empty($list) ) {
    foreach ($list as $key => $item) {
      $items[] = array(
        'title' => $item->post_title,
        'content' => $item->post_content,
        'url' => get_permalink( $item->ID ),
        'image' => get_the_post_thumbnail_url($item->ID,'full'),
        'number' => $images_url . $number_img[$key%4],
      );
    }
  }
  return $items;
}

function get_list_reviewers($list)
{
  $items = [];
  if (!empty($list) ) {
    foreach ($list as $item) {
      $items[] = array(
        'title' => $item->post_title,
        'content' => $item->post_content,
        'url' => get_permalink( $item->ID ),
        'age' => get_field('reviewer_age', $item->ID),
        'image' => get_the_post_thumbnail_url($item->ID,'full')
      );
    }
  }
  return $items;
}

function get_list_news($list)
{
  $items = [];
  if (!empty($list) ) {
    foreach ($list as $item) {
      $items[] = get_item_news($item);
    }
  }
  return $items;
}

function get_item_news($item) {
  $result = array(
    'title' => $item->post_title,
    'content' => $item->post_content,
    'url' => get_permalink( $item->ID ),
    'date' => get_the_date( 'd/m/y', $item->ID ),
    'comments_number' => get_comments_number($item->ID),
    'image' => get_the_post_thumbnail_url($item->ID, 'full')
  );
  return $result;
}

// Numbered Pagination
if ( !function_exists( 'wp_pagination' ) ) {
	
	function wp_pagination() {
		
		$prev_arrow = is_rtl() ? '→' : '←';
		$next_arrow = is_rtl() ? '←' : '→';
		
    global $wp_query;
		$total = $wp_query->max_num_pages;
		$big = 999999999; // need an unlikely integer
		if( $total > 1 )  {
			 if( !$current_page = get_query_var('paged') )
				 $current_page = 1;
			 if( get_option('permalink_structure') ) {
				 $format = 'page/%#%/';
			 } else {
				 $format = '&paged=%#%';
			 }
			$result = paginate_links(array(
				'base'			=> str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
				'format'		=> $format,
				'current'		=> max( 1, get_query_var('paged') ),
				'total' 		=> $total,
				'mid_size'		=> 3,
				'type' 			=> 'list',
				'prev_text'		=> $prev_arrow,
				'next_text'		=> $next_arrow,
       ) );
      echo str_replace( "<ul class='page-numbers'>", '<ul class="page-numbers pagination">', $result );
		}
	}
	
}
