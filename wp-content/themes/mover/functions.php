<?php
// Backend only
include_once( __DIR__ . '/lib/class-theme-init.php' );

// Global helpers
require_once( __DIR__ . '/lib/helpers/wordpress.php' );
require_once( __DIR__ . '/lib/helpers/modules.php' );
require_once( __DIR__ . '/lib/helpers/scripts.php' );
require_once( __DIR__ . '/lib/helpers/media.php' );
require_once( __DIR__ . '/lib/helpers/acf.php' );
require_once( __DIR__ . '/lib/helpers/data/homepage.php' );

function remove_menus() {
  remove_menu_page('edit.php?post_type=tin_tuc');
}
add_action('admin_menu', 'remove_menus');


// [pdf link="https://drive.google.com/file/d/0B7PqGrg9aj4ybmh3Sm42NmxhZnM/preview" width="550", height="550"]
function pdf_shortcode_func( $atts ) {
	$a = shortcode_atts( array(
		'link' => 'link',
		'width' => 'width',
		'height' => 'height'
	), $atts );

	return '<div id="Iframe-Cicis-Menu-To-Go" class="set-margin-cicis-menu-to-go set-padding-cicis-menu-to-go set-border-cicis-menu-to-go set-box-shadow-cicis-menu-to-go center-block-horiz">
		<div class="responsive-wrapper responsive-wrapper-padding-bottom-90pct" style="-webkit-overflow-scrolling: touch; overflow: auto;">
			<iframe src="'.$a['link'].'">
				<p style="font-size: 110%;"><em><strong>ERROR: </strong> An &#105;frame should be displayed here but your browser version does not support &#105;frames.</em> Please update your browser to its most recent version and try again, or access the file <a href="'.$a['link'].'"with this link.</a></p>
			</iframe>
		</div>
	</div>';
}
add_shortcode( 'pdf', 'pdf_shortcode_func' );


/*Add this code to your functions.php file of current theme OR plugin file if you're making a plugin*/
//add the button to the tinymce editor
add_action('media_buttons_context','add_my_tinymce_media_button');
function add_my_tinymce_media_button($context){

return $context.=__("
	<a href=\"#TB_inline?width=480&inlineId=my_shortcode_popup&width=640&height=513\" class=\"button thickbox\" id=\"my_shortcode_popup_button\" title=\"PDF Content\">PDF Content</a>");
}

add_action('admin_footer','my_shortcode_media_button_popup');
//Generate inline content for the popup window when the "my shortcode" button is clicked
function my_shortcode_media_button_popup(){?>
  <div id="my_shortcode_popup" style="display:none;">
    <--".wrap" class div is needed to make thickbox content look good-->
    <div class="wrap">
      <div>
        <h2>Add PDF Preview</h2>
        <div class="my_shortcode_add">
          <input type="text" id="id_of_textbox_user_typed_in" size="70px">
		  <button class="button-primary" id="id_of_button_clicked">Add URL</button>
        </div>
      </div>
    </div>
  </div>
<?php
}

//javascript code needed to make shortcode appear in TinyMCE edtor
add_action('admin_footer','my_shortcode_add_shortcode_to_editor');
function my_shortcode_add_shortcode_to_editor(){?>
<script>
jQuery('#id_of_button_clicked ').on('click',function(){
  var user_content = jQuery('#id_of_textbox_user_typed_in').val();
  var shortcode = '[pdf link="'+user_content+'"/]';
  if( !tinyMCE.activeEditor || tinyMCE.activeEditor.isHidden()) {
    jQuery('textarea#content').val(shortcode);
  } else {
    tinyMCE.execCommand('mceInsertContent', false, shortcode);
  }
  //close the thickbox after adding shortcode to editor
  self.parent.tb_remove();
});
</script>
<?php
}
